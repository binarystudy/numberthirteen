import React, {useEffect, useState} from 'react';
import {Alert} from 'react-native';
import {useDispatch} from 'react-redux';
import Contacts from 'react-native-contacts';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Home} from './home';
import {DetailContact} from './detailContact';
import {Loader} from './common';
import {requestContactsPermission} from '../helpers';
import {addContacts} from '../store';

const Stack = createStackNavigator();

const Root = () => {
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    const request = async () => {
      const permission = requestContactsPermission();
      if (!permission) {
        Alert.alert('Error', 'You not have permission to read contacts', [
          {text: 'Cancel'},
        ]);
        return;
      }
      const contacts = await Contacts.getAll();
      dispatch(addContacts(contacts));
      setLoading(false);
    };
    request();
  }, [dispatch]);
  return loading ? (
    <Loader />
  ) : (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{title: 'Contacts'}}
        />
        <Stack.Screen
          name="DetailContact"
          component={DetailContact}
          options={{title: 'Detail info'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Root;
