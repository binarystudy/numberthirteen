export const getEmail = emails => {
  const email = emails.reduce((prev, value) => {
    if (prev.length) {
      return prev;
    }
    return value?.email ? value.email : prev;
  }, '');
  return Boolean(email.length) ? email : '-';
};
