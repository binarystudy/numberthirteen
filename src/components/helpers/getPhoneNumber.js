export const getPhoneNumber = phones => {
  const phone = phones.reduce((prev, value) => {
    if (prev.length) {
      return prev;
    }
    return value?.number ? value.number : prev;
  }, '');
  return Boolean(phone.length) ? phone : '-';
};
