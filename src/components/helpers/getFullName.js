export const getFullName = ({familyName, givenName}) => {
  const name = `${familyName ? familyName : ''}${
    givenName ? ' ' + givenName : ''
  }`;
  return name.length ? name : '-';
};
