import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const Avatar = ({uri, size, nameIcon}) => {
  return (
    <View style={styles.root}>
      {uri ? (
        <Image source={{uri}} style={{width: size, height: size}} />
      ) : (
        <Icon style={styles.icon} name={nameIcon} size={size} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  icon: {
    color: '#616161',
  },
});
