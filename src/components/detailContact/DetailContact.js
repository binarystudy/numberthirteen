import React from 'react';
import {useSelector} from 'react-redux';
import {View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import call from 'react-native-phone-call';
import {Avatar} from '../common';
import {getFullName, getPhoneNumber} from '../helpers';
import {AvatarNameIcon, Icons, AvatarSizeIcon, IconsSize} from '../../config';

export const DetailContact = ({navigation, route}) => {
  const {list} = useSelector(state => state.contacts);
  const contact = list.find(
    contact => contact.recordID === route?.params?.recordID,
  );
  const {familyName, givenName, phoneNumbers, recordID, thumbnailPath} =
    contact || {};
  const {size, avatarWrapper, buttons, color, buttonTitle} = styles;

  const phoneNumber = (phoneNumbers && getPhoneNumber(phoneNumbers)) || '';

  const handleDeleteContact = () => {
    navigation.navigate('Home', {
      recordID,
    });
  };

  const handleCall = async () => {
    if (!phoneNumber.length) {
      return;
    }
    const args = {
      number: phoneNumber,
      prompt: false,
    };

    await call(args);
  };
  return (
    <View style={styles.root}>
      <View style={avatarWrapper}>
        <Avatar
          size={AvatarSizeIcon.DETAIL}
          nameIcon={AvatarNameIcon.DETAIL}
          uri={thumbnailPath}
        />
      </View>
      <View style={styles.group}>
        <Text style={[styles.label, size, color]}>Name</Text>
        <Text style={[styles.content, size, color]}>
          {familyName && givenName && getFullName({familyName, givenName})}
        </Text>
      </View>
      <View style={styles.group}>
        <Text style={[styles.label, size, color]}>Phone number</Text>
        <Text style={[styles.content, size, color]}>{phoneNumber}</Text>
      </View>
      <View style={styles.footer}>
        <Button
          icon={
            <Icon name={Icons.CALL} size={IconsSize.DETAIL} color="white" />
          }
          title="Call"
          titleStyle={[size, buttonTitle]}
          buttonStyle={[buttons, size]}
          onPress={handleCall}
        />
        <Button
          icon={
            <Icon name={Icons.DELETE} size={IconsSize.DETAIL} color="white" />
          }
          title="Delete"
          titleStyle={[size, buttonTitle]}
          buttonStyle={[buttons, size]}
          onPress={handleDeleteContact}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    paddingHorizontal: 10,
    marginHorizontal: 20,
  },
  avatarWrapper: {
    width: '100%',
    alignItems: 'center',
    marginVertical: 25,
  },
  size: {
    fontSize: 22,
  },
  color: {
    color: '#616161',
  },
  group: {
    marginBottom: 20,
  },
  label: {
    marginBottom: 10,
  },
  content: {
    borderBottomColor: '#616161',
    borderBottomWidth: 1,
    paddingBottom: 5,
    width: '100%',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  buttons: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: '#616161',
  },
  buttonTitle: {
    marginLeft: 10,
  },
});
