import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Avatar} from '../common';
import {getEmail, getFullName} from '../helpers';
import {AvatarNameIcon, Icons, AvatarSizeIcon, IconsSize} from '../../config';

export const Contact = ({contact, onDetailContact, onDeleteContact}) => {
  const {familyName, givenName, emailAddresses, recordID} = contact;
  const {color} = styles;
  return (
    <View style={styles.root}>
      <View style={styles.content}>
        <View style={[styles.contentIcon, color]}>
          <Avatar
            size={AvatarSizeIcon.LIST_ITEM}
            nameIcon={AvatarNameIcon.LIST_ITEM}
            uri={contact.thumbnailPath}
          />
        </View>
        <View>
          <Text style={[styles.name, color]}>
            {getFullName({familyName, givenName})}
          </Text>
          <Text>{getEmail(emailAddresses)}</Text>
        </View>
      </View>
      <View style={styles.icons}>
        <Icon
          style={[styles.delete, color]}
          name={Icons.DELETE}
          onPress={() => onDeleteContact(recordID)}
          size={IconsSize.CONTACT}
        />
        <Icon
          style={[color]}
          name={Icons.NAVIGATE_NEXT}
          onPress={() => onDetailContact(recordID)}
          size={IconsSize.CONTACT}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    backgroundColor: '#e7e9f4',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 15,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  color: {
    color: '#616161',
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentIcon: {
    marginRight: 10,
  },
  name: {
    fontSize: 18,
  },
  icons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  delete: {
    marginRight: 20,
  },
});
