import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {View, StyleSheet, FlatList, Alert} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Contacts from 'react-native-contacts';
import {Search} from './Search';
import {Contact} from './Contact';
import {Loader} from '../common';
import {addContact, deleteContact, addContacts} from '../../store';
import {SearchRegexp} from '../../config';

export const Home = ({route}) => {
  const [loading, setLoading] = useState(false);
  const {list} = useSelector(state => state.contacts);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    const recordID = route?.params?.recordID;
    if (!recordID) {
      return;
    }
    handleDeleteContact(recordID);
  }, [route?.params?.recordID]);

  const handleAddContact = async () => {
    try {
      setLoading(true);
      const contact = await Contacts.openContactForm({});
      setLoading(false);
      if (!contact) {
        return;
      }
      dispatch(addContact(contact));
    } catch (e) {
      Alert.alert('Error', 'Something wrong', [{text: 'Cancel'}]);
    }
  };

  const handleDetailContact = recordID => {
    navigation.navigate({name: 'DetailContact', params: {recordID}});
  };

  const handleDeleteContact = async recordID => {
    setLoading(true);
    await Contacts.deleteContact({recordID});
    setLoading(false);
    dispatch(deleteContact(recordID));
  };

  const handleSearch = async text => {
    const phoneNumberRegex = SearchRegexp.NAME;
    const emailAddressRegex = SearchRegexp.EMAIL;
    let contacts;
    if (text === '' || text === null) {
      contacts = await Contacts.getAll();
    } else if (phoneNumberRegex.test(text)) {
      contacts = await Contacts.getContactsByPhoneNumber(text);
    } else if (emailAddressRegex.test(text)) {
      contacts = await Contacts.getContactsByEmailAddress(text);
    } else {
      contacts = await Contacts.getContactsMatchingString(text);
    }
    dispatch(addContacts(contacts));
  };

  return loading ? (
    <Loader />
  ) : (
    <View style={styles.root}>
      <Search onPressAdd={() => handleAddContact()} onChange={handleSearch} />
      <FlatList
        data={list}
        renderItem={({item}) => (
          <Contact
            contact={item}
            onDetailContact={handleDetailContact}
            onDeleteContact={handleDeleteContact}
          />
        )}
        keyExtractor={item => item.recordID}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    paddingHorizontal: 10,
  },
});
