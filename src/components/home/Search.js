import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Icons, IconsSize} from '../../config';

export const Search = ({onChange, onPressAdd}) => {
  {
    return (
      <View style={styles.root}>
        <Icon
          style={styles.iconSearch}
          name={Icons.SEARCH}
          size={IconsSize.SEARCH}
        />
        <TextInput style={styles.input} onChangeText={onChange} />
        <Icon
          style={styles.iconAdd}
          name={Icons.PERSON_ADD}
          size={IconsSize.SEARCH}
          onPress={onPressAdd}
        />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    paddingVertical: 5,
    paddingRight: 60,
    paddingLeft: 45,
    marginBottom: 10,
  },
  iconSearch: {
    position: 'absolute',
    left: 10,
    top: 8,
    color: '#616161',
  },
  input: {
    padding: 0,
    fontSize: 24,
    width: '100%',
    color: '#616161',
  },
  iconAdd: {
    position: 'absolute',
    top: 4,
    right: 4,
    backgroundColor: '#616161',
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: 'white',
  },
});
