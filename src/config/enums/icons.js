export const Icons = {
  CALL: 'call',
  DELETE: 'delete',
  NAVIGATE_NEXT: 'navigate-next',
  PERSON_ADD: 'person-add',
  SEARCH: 'search',
};

export const IconsSize = {
  CONTACT: 36,
  DETAIL: 22,
  SEARCH: 24,
};
