export const AvatarNameIcon = {
  LIST_ITEM: 'perm-contact-calendar',
  DETAIL: 'portrait',
};

export const AvatarSizeIcon = {
  LIST_ITEM: 36,
  DETAIL: 150,
};
