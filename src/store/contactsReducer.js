import {createAction, createReducer} from '@reduxjs/toolkit';

export const addContact = createAction('contact/add');
export const deleteContact = createAction('contacts/delete');
export const addContacts = createAction('contacts/addList');

const initialState = {
  list: [],
};

export const contactsReducer = createReducer(initialState, builder => {
  builder
    .addCase(addContact, (state, action) => {
      state.list = [...state.list, action.payload];
    })
    .addCase(addContacts, (state, action) => {
      state.list = [...action.payload];
    })
    .addCase(deleteContact, (state, action) => {
      state.list = state.list.filter(
        contact => contact.recordID !== action.payload,
      );
    });
});
