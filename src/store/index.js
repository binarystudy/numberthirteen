import {configureStore, combineReducers} from '@reduxjs/toolkit';
import {contactsReducer} from './contactsReducer';

const rootReducer = combineReducers({
  contacts: contactsReducer,
});

const store = configureStore({
  reducer: rootReducer,
});

export {store};
export * from './contactsReducer';
